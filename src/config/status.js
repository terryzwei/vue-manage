export default {
    edit: 'edit',
    exchange: 'exchange',
    absorb: 'absorb',
    trace: 'trace',
    estatus: 'estatus'
}