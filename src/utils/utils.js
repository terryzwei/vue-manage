export const DateFormat = (datetime) => {
    if (!datetime) {
        datetime = new Date().getTime()
    }

    let year = new Date(datetime).getFullYear()
    year = year > 9 ? year : '0' + year
    let month = new Date(datetime).getMonth() + 1
    month = month > 9 ? month : '0' + month
    let date = new Date(datetime).getDate()
    date = date > 9 ? date : '0' + date

    return year + '-' + month + '-' + date
}