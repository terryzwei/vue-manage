module.exports = {
    devServer: {
        proxy: {
            '/api': {
                target: 'http://kumanxuan1.f3322.net:8084',
                // target: 'http://127.0.0.1:8086',
                // changeOrigin: true,
                pathRewrite: {
                    '^/api/coding/': '/'
                }
            }
        }
    }
}